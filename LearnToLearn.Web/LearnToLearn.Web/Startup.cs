﻿using AutoMapper;
using LearnToLearn.Data.Context;
using LearnToLearn.Data.Entities;
using LearnToLearn.Data.Repositories.Implementations;
using LearnToLearn.Data.Repositories.Interfaces;
using LearnToLearn.Service.Services.Implementations;
using LearnToLearn.Service.Services.Interfaces;
using LearnToLearn.Web.Models;
using LearnToLearn.Web.Models.EditModels;
using LearnToLearn.Web.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace LearnToLearn.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<LearnToLearnContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("LearnToLearnDb")));

            services.AddAutoMapper();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IUserRepo, UserRepo>();
            services.AddTransient<ICourseService, CourseService>();
            services.AddTransient<ICourseRepo, CourseRepo>();
            services.AddTransient<IEnrollmentRepo, EnrollmentRepo>();
            services.AddTransient<IEnrollmentService, EnrollmentService>();
            services.AddTransient<ITokenManager, TokenManager>();

            Mapper.Initialize(config =>
            {
                config.CreateMap<User, LoginModel>();
                config.CreateMap<User, RegisterModel>();
                config.CreateMap<User, UserModel>();
                config.CreateMap<User, UserEditModel>();
                config.CreateMap<Course, CourseModel>();
                config.CreateMap<Course, CourseEditModel>();
                config.CreateMap<Enrollment, EnrollmentModel>();
            });

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
               .AddJwtBearer(options =>
               {
                   options.TokenValidationParameters = new TokenValidationParameters
                   {
                       ValidateIssuer = true,
                       ValidateAudience = true,
                       ValidateLifetime = true,
                       ValidateIssuerSigningKey = true,
                       ValidIssuer = Configuration["Jwt:Issuer"],
                       ValidAudience = Configuration["Jwt:Issuer"],
                       IssuerSigningKey = new SymmetricSecurityKey(
                           Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                   };
               });

            services.AddDistributedRedisCache(r => { r.Configuration = Configuration["redis:connectionString"]; });

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();

            app.UseMvc();
        }
    }
}