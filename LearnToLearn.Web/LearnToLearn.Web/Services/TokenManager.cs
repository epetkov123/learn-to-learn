﻿using LearnToLearn.Web.Models;
using LearnToLearn.Web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace LearnToLearn.Web.Services
{
    public class TokenManager : ITokenManager
    {
        private IConfiguration _config;
        private readonly IDistributedCache _cache;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public TokenManager(IDistributedCache cache,
                IHttpContextAccessor httpContextAccessor,
                IConfiguration config
            )
        {
            _cache = cache;
            _httpContextAccessor = httpContextAccessor;
            _config = config;
        }


        public string GenerateToken(LoginModel loginModel, int id, string role)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var signinCredentials = new SigningCredentials(key,
                SecurityAlgorithms.HmacSha256);

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, id.ToString()),
                new Claim(JwtRegisteredClaimNames.Sub, loginModel.Email),
                new Claim(ClaimTypes.Role, role)
            };

            var token = new JwtSecurityToken(
                _config["Jwt:Issuer"],
                _config["Jwt:Issuer"],
                claims: claims,
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: signinCredentials
                );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public async Task DeactivateCurrentToken()
            => await Deactivate(GetCurrentToken());

        public async Task Deactivate(string token)
            => await _cache.SetStringAsync(GetTokenKey(token),
                " ", new DistributedCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow =
                        TimeSpan.FromMinutes(30)
                });

        private string GetCurrentToken()
        {
            var authorizationHeader = _httpContextAccessor
                .HttpContext.Request.Headers["authorization"];

            return authorizationHeader == StringValues.Empty
                ? string.Empty
                : authorizationHeader.Single().Split(" ").Last();
        }

        private static string GetTokenKey(string token)
            => $"tokens:{token}:deactivated";

        public int GetUserId()
        {
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            var token = handler.ReadToken(GetCurrentToken()) as JwtSecurityToken;

            return int.Parse(token.Claims.First(c => c.Type == "jti").Value);
        }
    }
}