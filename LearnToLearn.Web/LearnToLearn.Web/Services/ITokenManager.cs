﻿using LearnToLearn.Web.Models;
using System.Threading.Tasks;

namespace LearnToLearn.Web.Services
{
    public interface ITokenManager
    {
        string GenerateToken(LoginModel loginModel, int userId, string Role);

        Task DeactivateCurrentToken();

        Task Deactivate(string token);

        int GetUserId();
    }
}