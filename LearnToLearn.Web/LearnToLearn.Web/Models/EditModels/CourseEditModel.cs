﻿using System;

namespace LearnToLearn.Web.Models.EditModels
{
    public class CourseEditModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int TeacherId { get; set; }

        public bool? IsVisible { get; set; }

        public int? Capacity { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}