﻿using LearnToLearn.Data.Entities;
using System.ComponentModel.DataAnnotations;

namespace LearnToLearn.Web.Models.EditModels
{
    public class UserEditModel
    {
        public int Id { get; set; }

        [DataType(DataType.Text)]
        public string Name { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }

        public Role Role { get; set; }
    }
}