﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LearnToLearn.Web.Models
{
    public class CourseModel
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        public string Description { get; set; }

        public int TeacherId { get; set; }

        [Required]
        public bool IsVisible { get; set; }

        [Required]
        public int Capacity { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}