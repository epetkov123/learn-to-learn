﻿using System.ComponentModel.DataAnnotations;

namespace LearnToLearn.Web.Models
{
    public class GradeModel
    {
        [Required]
        public double Score { get; set; }

        [Required]
        public string Description { get; set; }
    }
}