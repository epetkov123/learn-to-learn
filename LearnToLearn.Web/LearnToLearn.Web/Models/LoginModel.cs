﻿using System.ComponentModel.DataAnnotations;

namespace LearnToLearn.Web.Models
{
    public class LoginModel
    {
        [Required(AllowEmptyStrings = false)]
        [EmailAddress]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}