﻿using LearnToLearn.Data.Entities;
using System;

namespace LearnToLearn.Web.Models
{
    public class EnrollmentModel
    {
        public int Id { get; set; }

        public User user { get; set; }
        public int UserId { get; set; }

        public Course course { get; set; }
        public int CourseId { get; set; }

        public Grade Grade { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}