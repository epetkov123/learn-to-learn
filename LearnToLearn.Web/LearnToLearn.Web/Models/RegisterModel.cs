﻿using LearnToLearn.Data.Entities;
using System.ComponentModel.DataAnnotations;

namespace LearnToLearn.Web.Models
{
    public class RegisterModel
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(AllowEmptyStrings = false)]
        [EmailAddress]
        public string Email { get; set; }

        public Role Role { get; set; }
    }
}