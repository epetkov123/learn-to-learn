﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using LearnToLearn.Data.Entities;
using LearnToLearn.Service.Helpers;
using LearnToLearn.Service.Services.Implementations;
using LearnToLearn.Web.Models;
using LearnToLearn.Web.Models.EditModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using static LearnToLearn.Data.Tools.Enumerations;

namespace LearnToLearn.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [Authorize(Roles = "Admin")]
    public class UserController : Controller
    {
        private IUserService _service;

        private IMapper _mapper;

        public UserController(IUserService userService, IMapper mapper)
        {
            _service = userService;
            _mapper = mapper;
        }

        [HttpPost]
        public IActionResult Create([FromBody] UserModel model)
        {
            if (model == null)
            {
                return BadRequest("No user details given");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid user details");
            }

            if (_service.CheckIfUserExists(model.Email))
            {
                return BadRequest("User already exists");
            }

            var role = new Role
            {
                RoleName = RoleOptions.Teacher
            };

            model.Role = role;
            model.Password = PasswordHasher.Create(model.Password);

            var item = _mapper.Map(model, new User());

            _service.Create(item);
            _service.Save();

            return CreatedAtAction(nameof(Create), item);
        }

        [HttpGet]
        public List<UserModel> GetAll()
        {
            var users = _service.GetAll().ProjectTo<UserModel>().ToList();

            return users;
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var item = _mapper.Map(_service.GetById(id), new UserModel());

            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] UserEditModel model)
        {
            if (model == null)
            {
                return BadRequest("No user details given");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid user details");
            }

            var item = _service.GetById(id);
            if (item == null)
            {
                return NotFound();
            }

            if (model.Email != null)
            {
                item.Email = model.Email;
            }

            if (model.Name != null)
            {
                item.Name = model.Name;
            }

            if (model.Password != null)
            {
                item.Password = PasswordHasher.Create(model.Password);
            }

            _service.Update(item);
            _service.Save();

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (id == 1)
            {
                return BadRequest("Cannot delete admin");
            }

            if (_service.GetById(id) == null)
            {
                return NotFound();
            }

            _service.Delete(id);
            _service.Save();

            return Ok();
        }
    }
}