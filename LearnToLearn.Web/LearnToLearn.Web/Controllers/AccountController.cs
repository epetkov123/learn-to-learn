﻿using AutoMapper;
using LearnToLearn.Data.Entities;
using LearnToLearn.Service.Helpers;
using LearnToLearn.Service.Services.Implementations;
using LearnToLearn.Web.Models;
using LearnToLearn.Web.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using static LearnToLearn.Data.Tools.Enumerations;

namespace LearnToLearn.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private IUserService _service;
        private ITokenManager _tokenManager;
        private IMapper _mapper;

        public AccountController(IUserService service, IMapper mapper, ITokenManager tokenManager)
        {
            _service = service;
            _mapper = mapper;
            _tokenManager = tokenManager;
        }

        [HttpPost("register")]
        [AllowAnonymous]
        public IActionResult Register([FromBody] RegisterModel model)
        {
            if (model == null)
            {
                return BadRequest("No registration details given");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid registration details");
            }

            if (_service.CheckIfUserExists(model.Email))
            {
                return BadRequest("User already exits");
            }

            model.Role = new Role
            {
                RoleName = RoleOptions.Normal
            };
            model.Password = PasswordHasher.Create(model.Password);

            _service.Create(_mapper.Map(model, new User()));
            _service.Save();

            EmailSender.sendToEmail(model.Name, model.Email);

            return Ok();
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public IActionResult Login([FromBody]LoginModel model)
        {
            if (User.Identity.IsAuthenticated)
            {
                return BadRequest("Already logged in");
            }

            if (model == null)
            {
                return BadRequest("No login details given");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid login details");
            }

            var hashedPassword = _service.GetByEmail(model.Email).Password;

            if (_service.GetByEmail(model.Email) == null
                && PasswordHasher.Validate(model.Password, hashedPassword))
            {
                return BadRequest("Invalid login");
            }

            var role = _service.GetByEmail(model.Email).Role.RoleName;
            var userId = _service.GetByEmail(model.Email).Id;

            var tokenString = _tokenManager.GenerateToken(model, userId, role.ToString());

            return Ok(new { token = tokenString });
        }

        [HttpGet("logout")]
        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await _tokenManager.DeactivateCurrentToken();

            return Ok();
        }
    }
}