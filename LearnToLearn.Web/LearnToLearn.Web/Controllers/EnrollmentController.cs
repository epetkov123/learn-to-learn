﻿using AutoMapper;
using LearnToLearn.Web.Services;
using LearnToLearn.Service.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using LearnToLearn.Web.Models;
using System;
using LearnToLearn.Data.Entities;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using AutoMapper.QueryableExtensions;
using System.Linq;

namespace LearnToLearn.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Enrollment")]
    public class EnrollmentController : Controller
    {
        private IEnrollmentService _service;

        private ICourseService _courseService;

        private IMapper _mapper;

        private ITokenManager _tokenManager;

        public EnrollmentController(IEnrollmentService service,
            IMapper mapper,
            ITokenManager tokenManager,
            ICourseService courseService)
        {
            _service = service;
            _courseService = courseService;
            _mapper = mapper;
            _tokenManager = tokenManager;
        }

        [HttpPost]
        [Authorize(Roles = "Admin, Normal")]
        public IActionResult Create([FromBody] EnrollmentModel model)
        {
            if (model == null)
            {
                return BadRequest("No enrollment details given");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid enrollment details");
            }

            var course = _courseService.GetById(model.CourseId);

            if (course == null)
            {
                return BadRequest("Course does not exist");
            }

            var grade = new Grade
            {
                Description = "Not graded"
            };

            model.Grade = grade;
            model.CreatedAt = DateTime.Now;
            model.UpdatedAt = DateTime.Now;
            model.UserId = _tokenManager.GetUserId();

            var item = _mapper.Map(model, new Enrollment());

            _service.Create(item);
            _service.Save();

            return CreatedAtAction(nameof(Create), item);
        }

        [HttpGet]
        [Authorize]
        public List<EnrollmentModel> GetAll()
        {
            var userId = _tokenManager.GetUserId();

            return _service.GetAll()
                .ProjectTo<EnrollmentModel>()
                .Where(e => e.UserId == userId)
                .ToList();
        }

        [HttpGet("{id}")]
        [Authorize]
        public IActionResult GetById(int id)
        {
            var item = _mapper.Map(_service.GetById(id), new EnrollmentModel());

            if (item == null)
            {
                return NotFound();
            }

            if (item.UserId != _tokenManager.GetUserId())
            {
                return BadRequest("Cannot show enrollments that are not yours");
            }

            return Ok(item);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin, Normal")]
        public IActionResult Delete(int id)
        {
            var item = _mapper.Map(_service.GetById(id), new EnrollmentModel());

            if (item == null)
            {
                return NotFound();
            }

            _service.Delete(id);
            _service.Save();

            return Ok();
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "Admin, Teacher")]
        public IActionResult Update([FromBody] GradeModel model, int id)
        {
            if (model == null)
            {
                return BadRequest("No grading details given");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid grading details");
            }

            var item = _service.GetByIdWithGrade(id);
            if (item == null)
            {
                return NotFound();
            }

            if (_courseService.GetById(item.CourseId).TeacherId != _tokenManager.GetUserId())
            {
                return BadRequest("Cannot grade enrollments on courses that are not yours");
            }

            item.Grade.Score = model.Score;
            item.Grade.Description = model.Description;
            item.UpdatedAt = DateTime.Now;

            _service.Update(item);
            _service.Save();

            return Ok();
        }
    }
}