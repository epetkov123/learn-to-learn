﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using LearnToLearn.Data.Entities;
using LearnToLearn.Service.Services.Interfaces;
using LearnToLearn.Web.Models;
using LearnToLearn.Web.Models.EditModels;
using LearnToLearn.Web.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LearnToLearn.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class CourseController : Controller
    {
        private ICourseService _service;

        private IMapper _mapper;

        private ITokenManager _tokenManager;

        public CourseController(ICourseService service, IMapper mapper, ITokenManager tokenManager)
        {
            _service = service;
            _mapper = mapper;
            _tokenManager = tokenManager;
        }

        [HttpPost]
        [Authorize(Roles = "Admin, Teacher")]
        public IActionResult Create([FromBody] CourseModel model)
        {
            if (model == null)
            {
                return BadRequest("No course details given");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid course details");
            }

            if (_service.CheckIfCourseExists(model.Name))
            {
                return BadRequest("Course with the same name already exists");
            }

            model.CreatedAt = DateTime.Now;
            model.UpdatedAt = DateTime.Now;
            model.TeacherId = _tokenManager.GetUserId();

            var item = _mapper.Map(model, new Course());

            _service.Create(item);
            _service.Save();

            return CreatedAtAction(nameof(Create), item);
        }

        [HttpGet]
        [Authorize]
        public List<CourseModel> GetAll()
        {
            return _service.GetAll()
                .ProjectTo<CourseModel>()
                .Where(c => c.IsVisible == true)
                .ToList();
        }

        [HttpGet("{id}")]
        [Authorize]
        public IActionResult GetById(int id)
        {
            var item = _mapper.Map(_service.GetById(id), new CourseModel());

            if (item == null)
            {
                return NotFound();
            }

            if (!item.IsVisible)
            {
                return BadRequest("Item is not visible");
            }

            return Ok(item);
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "Admin, Teacher")]
        public IActionResult Update(int id, [FromBody] CourseEditModel model)
        {
            if (model == null)
            {
                return BadRequest("No course details given");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid user details");
            }

            var item = _service.GetById(id);
            if (item == null)
            {
                return NotFound();
            }

            if (item.TeacherId != _tokenManager.GetUserId())
            {
                return BadRequest("Cannot edit courses that are not yours");
            }

            if (model.Name != null)
            {
                item.Name = model.Name;
            }

            if (model.IsVisible != null)
            {
                item.IsVisible = (bool)model.IsVisible;
            }

            if (model.Description != null)
            {
                item.Description = model.Description;
            }

            if (model.Capacity != null)
            {
                item.Capacity = (int)model.Capacity;
            }

            item.UpdatedAt = DateTime.Now;

            _service.Update(item);
            _service.Save();

            return Ok();
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin, Teacher")]
        public IActionResult Delete(int id)
        {
            if (_service.GetById(id) == null)
            {
                return NotFound();
            }

            var item = _mapper.Map(_service.GetById(id), new Course());

            if (item.TeacherId != _tokenManager.GetUserId())
            {
                return BadRequest("Cannot delete courses that are not yours");
            }

            _service.Delete(id);
            _service.Save();

            return Ok();
        }
    }
}