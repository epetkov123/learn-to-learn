﻿using Microsoft.AspNetCore.Mvc;

namespace LearnToLearn.Web.Controllers
{
    [Route("")]
    public class HomeController : Controller
    {
        [HttpGet]
        public IActionResult Get()
            => Content("LearnToLearn API");
    }
}