﻿using System.Linq;

namespace LearnToLearn.Service.Services.Interfaces
{
    public interface IBaseService<T>
    {
        void Create(T item);

        IQueryable GetAll();

        T GetById(int id);

        void Update(T item);

        void Delete(int id);

        void Save();
    }
}