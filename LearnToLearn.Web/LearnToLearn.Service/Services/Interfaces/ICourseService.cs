﻿using LearnToLearn.Data.Entities;

namespace LearnToLearn.Service.Services.Interfaces
{
    public interface ICourseService : IBaseService<Course>
    {
        bool CheckIfCourseExists(string name);
    }
}
