﻿using LearnToLearn.Data.Entities;

namespace LearnToLearn.Service.Services.Interfaces
{
    public interface IEnrollmentService : IBaseService<Enrollment>
    {
        Enrollment GetByIdWithGrade(int id);
    }
}