﻿using LearnToLearn.Data.Entities;
using LearnToLearn.Service.Services.Interfaces;

namespace LearnToLearn.Service.Services.Implementations
{
    public interface IUserService : IBaseService<User>
    {
        bool CheckIfUserExists(string email);

        User GetByEmail(string email);
    }
}