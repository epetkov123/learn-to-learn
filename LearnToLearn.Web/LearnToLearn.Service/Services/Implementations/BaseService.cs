﻿using LearnToLearn.Data.Entities;
using LearnToLearn.Data.Repositories.Interfaces;
using LearnToLearn.Service.Services.Interfaces;
using System.Linq;

namespace LearnToLearn.Service.Services.Implementations
{
    public class BaseService<T> : IBaseService<T> where T : BaseEntity
    {
        protected IBaseRepo<T> repository;

        public BaseService(IBaseRepo<T> repository)
        {
            this.repository = repository;
        }

        public void Create(T item)
        {
            repository.Create(item);
        }

        public IQueryable GetAll()
        {
            return repository.GetAll();
        }

        public T GetById(int id)
        {
            return repository.GetById(id);
        }

        public void Update(T item)
        {
            repository.Update(item);
        }

        public void Delete(int id)
        {
            repository.Delete(GetById(id));
        }

        public void Save()
        {
            repository.Save();
        }
    }
}