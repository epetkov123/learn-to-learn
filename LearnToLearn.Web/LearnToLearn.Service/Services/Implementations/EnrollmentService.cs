﻿using LearnToLearn.Data.Entities;
using LearnToLearn.Data.Repositories.Interfaces;

namespace LearnToLearn.Service.Services.Implementations
{
    public class EnrollmentService : BaseService<Enrollment>, Interfaces.IEnrollmentService
    {
        private IEnrollmentRepo _repo;

        public EnrollmentService(IEnrollmentRepo repo)
            : base(repo)
        {
            _repo = repo;
        }

        public Enrollment GetByIdWithGrade(int id)
        {
            return _repo.GetByIdWithGrade(id);
        }
    }
}