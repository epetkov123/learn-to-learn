﻿using LearnToLearn.Data.Entities;
using LearnToLearn.Data.Repositories.Interfaces;
using LearnToLearn.Service.Services.Interfaces;

namespace LearnToLearn.Service.Services.Implementations
{
    public class CourseService : BaseService<Course>, ICourseService
    {
        private ICourseRepo _repo;

        public CourseService(ICourseRepo repo)
            : base(repo)
        {
            _repo = repo;
        }

        public bool CheckIfCourseExists(string name)
        {
            return _repo.GetIfNameIsUnique(name);
        }
    }
}