﻿using LearnToLearn.Data.Entities;
using LearnToLearn.Data.Repositories.Interfaces;

namespace LearnToLearn.Service.Services.Implementations
{
    public class UserService : BaseService<User>, IUserService
    {
        private IUserRepo repo;

        public UserService(IUserRepo repo)
            : base(repo)
        {
            this.repo = repo;
        }

        public User GetByEmail(string email)
        {
            return repo.GetByEmail(email);
        }

        public bool CheckIfUserExists(string email)
        {
            return repo.GetIfEmailIsUnique(email);
        }
    }
}