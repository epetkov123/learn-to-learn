﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Text;

namespace LearnToLearn.Service.Helpers
{
    public class PasswordHasher
    {
        public static string Create(string password)
        {
            var valueBytes = KeyDerivation.Pbkdf2(
                                password: password,
                                salt: Encoding.UTF8.GetBytes("j41i24oj12oi"),
                                prf: KeyDerivationPrf.HMACSHA512,
                                iterationCount: 10000,
                                numBytesRequested: 256 / 8);

            return Convert.ToBase64String(valueBytes);
        }

        public static bool Validate(string password, string hash)
            => Create(password) == hash;
    }
}