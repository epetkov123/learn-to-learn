﻿using LearnToLearn.Data.Entities;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Text;
using static LearnToLearn.Data.Tools.Enumerations;

namespace LearnToLearn.Data.Context
{
    public class LearnToLearnContext : DbContext
    {
        public LearnToLearnContext(DbContextOptions options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Enrollment>()
                .HasOne(g => g.Grade)
                .WithOne(e => e.Enrollment)
                .HasForeignKey<Grade>(g => g.EnrollmentId)
                .OnDelete(DeleteBehavior.Cascade);
            
            
            modelBuilder.Entity<User>()
                .HasOne(r => r.Role)
                .WithOne(u => u.User)
                .HasForeignKey<Role>(r => r.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
                .HasData(new User
                {
                    Id = 1,
                    Name = "admin",
                    Email = "admin@learn2learn.com",
                    Password = HashPassword("123456")
                });

            modelBuilder.Entity<Role>()
                .HasData(new Role
                {
                    Id = 1,
                    RoleName = RoleOptions.Admin,
                    UserId = 1
                });

            base.OnModelCreating(modelBuilder);
        }

        public static string HashPassword(string password)
        {
            var valueBytes = KeyDerivation.Pbkdf2(
                                password: password,
                                salt: Encoding.UTF8.GetBytes("j41i24oj12oi"),
                                prf: KeyDerivationPrf.HMACSHA512,
                                iterationCount: 10000,
                                numBytesRequested: 256 / 8);

            return Convert.ToBase64String(valueBytes);
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Course> Courses { get; set; }

        public DbSet<Enrollment> Enrollments { get; set; }

        public DbSet<Grade> Grades { get; set; }

        public DbSet<Role> Roles { get; set; }
    }
}