﻿using LearnToLearn.Data.Entities;

namespace LearnToLearn.Data.Repositories.Interfaces
{
    public interface IEnrollmentRepo : IBaseRepo<Enrollment>
    {
        Enrollment GetByIdWithGrade(int id);
    }
}