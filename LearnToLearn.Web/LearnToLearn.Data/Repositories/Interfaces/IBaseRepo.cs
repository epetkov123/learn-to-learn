﻿using System.Linq;

namespace LearnToLearn.Data.Repositories.Interfaces
{
    public interface IBaseRepo<T>
    {
        void Create(T item);

        IQueryable GetAll();

        T GetById(int id);

        void Update(T item);

        void Delete(T item);

        void Save();
    }
}