﻿using LearnToLearn.Data.Entities;

namespace LearnToLearn.Data.Repositories.Interfaces
{
    public interface ICourseRepo : IBaseRepo<Course>
    {
        bool GetIfNameIsUnique(string name);
    }
}