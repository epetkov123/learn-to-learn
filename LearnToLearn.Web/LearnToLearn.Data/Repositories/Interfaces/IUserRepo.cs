﻿using LearnToLearn.Data.Entities;

namespace LearnToLearn.Data.Repositories.Interfaces
{
    public interface IUserRepo : IBaseRepo<User>
    {
        User GetByEmail(string email);

        bool GetIfEmailIsUnique(string email);
    }
}