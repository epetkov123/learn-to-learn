﻿using LearnToLearn.Data.Context;
using LearnToLearn.Data.Entities;
using LearnToLearn.Data.Repositories.Interfaces;
using System.Linq;

namespace LearnToLearn.Data.Repositories.Implementations
{
    public class CourseRepo : BaseRepo<Course>, ICourseRepo
    {
        public CourseRepo(LearnToLearnContext context)
            : base(context)
        {
        }

        public bool GetIfNameIsUnique(string name)
        {
            if ((context.Courses.Where(u => u.Name == name).Any()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}