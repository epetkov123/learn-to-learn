﻿using LearnToLearn.Data.Context;
using LearnToLearn.Data.Entities;
using LearnToLearn.Data.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace LearnToLearn.Data.Repositories.Implementations
{
    public class UserRepo : BaseRepo<User>, IUserRepo
    {
        public UserRepo(LearnToLearnContext context)
            : base(context)
        {
        }

        public User GetByEmail(string email)
        {
            return context.Users
                .Include(u => u.Role)
                .FirstOrDefault(u => u.Email == email);
        }

        public bool GetIfEmailIsUnique(string email)
        {
            if ((context.Users.Where(u => u.Email == email).Any()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}