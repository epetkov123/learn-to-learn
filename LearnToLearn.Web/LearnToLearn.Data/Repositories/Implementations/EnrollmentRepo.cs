﻿using LearnToLearn.Data.Context;
using LearnToLearn.Data.Entities;
using LearnToLearn.Data.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace LearnToLearn.Data.Repositories.Implementations
{
    public class EnrollmentRepo : BaseRepo<Enrollment>, IEnrollmentRepo
    {
        public EnrollmentRepo(LearnToLearnContext context)
            : base(context)
        {
        }

        public Enrollment GetByIdWithGrade(int id)
        {
            return context.Enrollments
                .Include(e => e.Grade)
                .FirstOrDefault(e => e.Id == id);
        }
    }
}