﻿using LearnToLearn.Data.Context;
using LearnToLearn.Data.Entities;
using LearnToLearn.Data.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace LearnToLearn.Data.Repositories
{
    public class BaseRepo<T> : IBaseRepo<T> where T : BaseEntity
    {
        protected LearnToLearnContext context;
        protected DbSet<T> dbSet;

        public BaseRepo(LearnToLearnContext context)
        {
            this.context = context;
            dbSet = context.Set<T>();
        }

        public void Create(T item)
        {
            dbSet.Add(item);
        }

        public IQueryable GetAll()
        {
            return dbSet;
        }

        public T GetById(int id)
        {
            return dbSet.Find(id);
        }

        public void Update(T item)
        {
            dbSet.Update(item);
        }

        public void Delete(T item)
        {
            dbSet.Remove(item);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}