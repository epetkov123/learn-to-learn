﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LearnToLearn.Data.Entities
{
    public class Course : BaseEntity
    {
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        public string Description { get; set; }

        public int TeacherId { get; set; }

        [Required]
        public bool IsVisible { get; set; }

        [Required]
        public int Capacity { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        [Required]
        public DateTime UpdatedAt { get; set; }
    }
}