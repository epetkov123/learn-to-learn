﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace LearnToLearn.Data.Entities
{
    public class Enrollment : BaseEntity
    {
        [JsonIgnore]
        public User user { get; set; }
        public int UserId { get; set; }

        [JsonIgnore]
        public Course course { get; set; }
        public int CourseId { get; set; }

        [Required]
        public Grade Grade { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }
        [Required]
        public DateTime UpdatedAt { get; set; }
    }
}