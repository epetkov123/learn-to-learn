﻿using Newtonsoft.Json;
using static LearnToLearn.Data.Tools.Enumerations;

namespace LearnToLearn.Data.Entities
{
    public class Role : BaseEntity
    {
        public RoleOptions RoleName { get; set; }

        public int UserId { get; set; }
        [JsonIgnore]
        public User User { get; set; }
    }
}