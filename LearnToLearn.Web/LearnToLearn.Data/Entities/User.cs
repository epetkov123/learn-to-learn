﻿using System.ComponentModel.DataAnnotations;

namespace LearnToLearn.Data.Entities
{
    public class User : BaseEntity
    {
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Password { get; set; }

        public Role Role { get; set; }
    }
}