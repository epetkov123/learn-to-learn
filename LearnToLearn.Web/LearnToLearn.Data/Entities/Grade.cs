﻿using Newtonsoft.Json;

namespace LearnToLearn.Data.Entities
{
    public class Grade : BaseEntity
    {
        public double Score { get; set; }

        public string Description { get; set; }

        public int EnrollmentId { get; set; }
        [JsonIgnore]
        public Enrollment Enrollment { get; set; }
    }
}